#!/bin/bash

PROCESSCWD=`pwd`
KHIPROOT=~/Music/other
KHIPLISTS=./z_playlists/khip

function main() {
	cd $KHIPROOT
	rm $KHIPLISTS/*
	echo "generating khip playlists in"
	echo $(cd "$KHIPLISTS"; pwd)
	lists
	cd $PROCESSCWD
}


function lists {
	
khip playlist \
	'"tagging:pkmntype" like "%Bug%"' $KHIPLISTS/type_bug.m3u
khip playlist \
	'"tagging:pkmntype" like "%Dark%"' $KHIPLISTS/type_dark.m3u
khip playlist \
	'"tagging:pkmntype" like "%Dragon%"' $KHIPLISTS/type_dragon.m3u
khip playlist \
	'"tagging:pkmntype" like "%Electric%"' $KHIPLISTS/type_electric.m3u
khip playlist \
	'"tagging:pkmntype" like "%Fairy%"' $KHIPLISTS/type_fairy.m3u
khip playlist \
	'"tagging:pkmntype" like "%Fighting%"' $KHIPLISTS/type_fighting.m3u
khip playlist \
	'"tagging:pkmntype" like "%Fire%"' $KHIPLISTS/khip/type_fire.m3u
khip playlist \
	'"tagging:pkmntype" like "%Flying%"' $KHIPLISTS/type_flying.m3u
khip playlist \
	'"tagging:pkmntype" like "%Ghost%"' $KHIPLISTS/type_ghost.m3u
khip playlist \
	'"tagging:pkmntype" like "%Grass%"' $KHIPLISTS/type_grass.m3u
khip playlist \
	'"tagging:pkmntype" like "%Ground%"' $KHIPLISTS/type_ground.m3u
khip playlist \
	'"tagging:pkmntype" like "%Ice%"' $KHIPLISTS/type_ice.m3u
khip playlist \
	'"tagging:pkmntype" like "%Normal%"' $KHIPLISTS/type_normal.m3u
khip playlist \
	'"tagging:pkmntype" like "%Poison%"' $KHIPLISTS/type_poison.m3u
khip playlist \
	'"tagging:pkmntype" like "%Psychic%"' $KHIPLISTS/type_psychic.m3u
khip playlist \
	'"tagging:pkmntype" like "%Rock%"' $KHIPLISTS/type_rock.m3u
khip playlist \
	'"tagging:pkmntype" like "%Steel%"' $KHIPLISTS/type_steel.m3u
khip playlist \
	'"tagging:pkmntype" like "%Water%"' $KHIPLISTS/type_water.m3u
	
}

main
