#!/usr/bin/env node

// requires {{{
const
fs = require('fs'), path = require('path')
, rl = require('readline')
, mm = require('music-metadata')
, countFiles = require('count-files')
, sqlite3 = require('sqlite3').verbose()
, uuidv5 = require('uuid/v5')
, Table = require('table')
;

// analyse uses a feature of fs.readdir only in node 10
// it can be installed with `nvm install 10`
// after you first install the nvm script

// }}}

// util etc {{{

var util = { // {{{
	
	substring: function(text) {
		if (text.substring) {
			return text.substring(0,150);
		}
		
		else { return text; }
	}
	
	,
	// https://stackoverflow.com/questions/147824
	isDoubleByte: function(s) {
		return /[^\u0000-\u00ff]/.test(s);
	}
	
	,
	// fix "~/" in file path
	// https://stackoverflow.com/questions/21077670
	resolveHome: function(filepath) {
		if (filepath[0] === '~') {
			filepath = filepath.replace(/^\~/, './');
			return path.join(process.env.HOME, filepath);
		}
		return filepath;
	}
	
}; // }}}

var sqlUtils = { // {{{
	COLUMN_TYPES: true, // not to be confused with the list of column types!
	
	// list of columns as in CREATE TABLE or ALTER TABLE
	columnList: function(columns, types) {
		var values = [], result, i, colType;
		const columnTypes = KHip.COLUMN_TYPES;
		
		if (types === undefined) {
			return '"' + columns.join('", "') + '"';
		}
		
		for (i = 0; i < columns.length; i++) {
			colType = 'text';
			
			if (columnTypes[columns[i]] !== undefined) {
				colType = columnTypes[columns[i]];
			}
			
			values.push('"' + columns[i] + '" ' + colType);
		}
		
		result = values.join(', ');
		
		return result;
	},
	
	// convert a column value in a list of row values to a string
	// suitable for putting into an insert/update statemetn
	columnValue: function(row, columnName) {
		const columnTypes = KHip.COLUMN_TYPES;
		
		// push null as null
		if (row[columnName] === undefined) {
			return 'null';
		}
		// if column type is in the array assume it's not quoted
		else if (columnTypes[columnName] !== undefined) {
			return row[columnName];
		}
		// otherwise assume the column is text and quote it
		else {
			return ('"' + (row[columnName] + '').replace(/"/g, '""').replace(/;/g, '\\;') + '"');
		}
	}
	
}; // }}}

// }}}

// globals / constants
var KHip = { // {{{
	NAMESPACE: '44656570-5370-6163-6550-6567-61737573',
	// DeepSpacePegasus
	
	COLUMN_TYPES: {
		'khip:filesize': 'integer',
		'tracknum': 'integer'
	},
	
	TAG_MAP: { // {{{
		// PRIV
		'mp3': {
			"APIC": 'picture'
			, "COMM": 'comment'
			, "MCDI": 'cdid'
			, "POPM": 'popularimeter'
			, "TALB": 'album'
			, "TBPM": 'bpm'
			, "TCOM": 'composer'
			, "TCON": 'genre'
			, "TCOP": 'copyright'
			, "TDEN": 'encodingtime'
			, "TENC": 'encodedby'
			, "TPE1": 'artist'
			, "TDRC": 'year'
			, "TIT1": 'contentgroup'
			, "TIT2": 'title'
			, "TIT3": 'subtitle'
			, "TOAL": 'originalalbum'
			, "TOPE": 'originalartist'
			, "TPE2": 'albumartist'
			, "TPUB": 'publisher'
			, "TRCK": 'tracknum'
			, "TSSE": 'software'
			, "WOAR": 'wwwartist'
			, "WOAS": 'wwwsource'
			, "WXXX": 'wwwuser'
		},
		"Ogg/Vorbis I": {
			"BAND": 'albumartist'
			, "DATE": 'year'
			, "ORIGALBUM": 'originalalbum'
			, "ORIGARTIST": 'originalartist'
		}
	}, // }}}
	
	
	inputDir: '',
	
	headershort: {
		'albumartist': 'a. artist',
		'originalalbum': 'o. album',
		'tagging:pkmntype': 't:type',
		'tagging:subject': 't:subject',
		'tagging:kaisort': 't:sort',
		'khip:youtube': 'youtube',
		'khip:filename': 'filename',
		'khip:uuid': 'uuid'
	},
	
	db: null,
	dbMeta: {
		filename: '',
		columns: [],
		uuid: []
	},
	
	defaults: {
		configDir: '~/.config/coelohippus',
		
		dbColumns: [ "khip:uuid", "khip:filename", "khip:filepath",  "khip:filesize", "khip:format", "title", "artist", "album", "year", "comment" ],
		analyseColumns: [ 'title', 'artist', 'album' ]
	},
	
	config: {
		//rootDir: params['KHIPROOT'],
		//listDir: params['KHIPLISTS'],
		//analyseColumns: params['ANALYSECOLUMNS'],
		
		//lists: lists
	}
	
}; // }}}

/* todo: {{{

	X read extended tags
		- write uuid? ~~music-metadata can't actually do this.~~
		  this would greatly help finding duplicates.
	X make database
		X update row when track exists
		- identify duplicates when filename changes & update
		
	analyse
		X analyse quietly (--quiet/-q)
		X store analyse sql statements
		- analyse progress bar
		X read config for analyse columns
		X choose max width based on terminal width
		- fix the dir not found issue
		- allow relative paths!!
		
	X generate playlists from database
		X ini file to generate sh that generates playlists
		- test short queries with multiple columns

}}} */

/* music-metadata examples {{{ */
// mp3 {{{
/* {
  "format": {
    "tagTypes": [
      "ID3v2.4",
      "ID3v1"
    ],
    "lossless": false,
    "dataformat": "mp3",
    "bitrate": 128000,
    "sampleRate": 44100,
    "numberOfChannels": 2,
    "codecProfile": "CBR",
    "encoder": "LAME 3.99.5",
    "duration": 196.75428571429
  },
  "native": {
    "ID3v2.4": [
      {
        "id": "TPE1",
        "value": "MidiMusicMaker"
      },
      {
        "id": "TIT2",
        "value": "Mega Man 5 - Proto Man Castle (Darkman Castle)"
      },
      {
        "id": "TDRC",
        "value": "2015"
      },
      {
        "id": "TOAL",
        "value": "Mega Man 5"
      },
      {
        "id": "TXXX:minor_version",
        "value": "0"
      },
      {
        "id": "TXXX:compatible_brands",
        "value": "isommp42"
      },
      {
        "id": "WOAS",
        "value": "https:\/\/www.youtube.com\/watch?v=_VJ0KTeqoQk\u0000"
      },
      {
        "id": "TXXX:major_brand",
        "value": "mp42"
      },
      {
        "id": "TXXX:tagging:pkmntype",
        "value": "Dark"
      },
      {
        "id": "TPE2",
        "value": "Mega Man"
      },
      {
        "id": "TSSE",
        "value": "Lavf57.20.100"
      },
      {
        "id": "TXXX:tagging:subject",
        "value": "Dark Man"
      }
    ],
    "ID3v1": [
      {
        "id": "title",
        "value": "Mega Man 5 - Proto Man Castle"
      },
      {
        "id": "artist",
        "value": "MidiMusicMaker"
      },
      {
        "id": "year",
        "value": "2015"
      }
    ]
  },
  "common": {
    "track": {
      "no": null,
      "of": null
    },
    "disk": {
      "no": null,
      "of": null
    },
    "artists": [
      "MidiMusicMaker"
    ],
    "artist": "MidiMusicMaker",
    "title": "Mega Man 5 - Proto Man Castle (Darkman Castle)",
    "year": 2015,
    "date": "2015",
    "originalalbum": "Mega Man 5",
    "albumartist": "Mega Man",
    "encodersettings": "Lavf57.20.100"
  }
} */
// }}}
// ogg {{{
/* {
  "format": {
    "tagTypes": [
      "vorbis"
    ],
    "dataformat": "Ogg\/Vorbis I", // {{{
    "sampleRate": 44100,
    "bitrate": 160000,
    "numberOfChannels": 2,
    "numberOfSamples": 13048320,
    "duration": 295.88027210884
  }, // }}}
  "native": {
    "vorbis": [
      {
        "id": "ARTIST",
        "value": "ABSRDST"
      },
      {
        "id": "MAJOR_BRAND",
        "value": "mp42"
      },
      {
        "id": "TAGGING:PKMNTYPE",
        "value": "Dark"
      },
      {
        "id": "MINOR_VERSION",
        "value": "0"
      },
      {
        "id": "ALBUMARTIST",
        "value": "Mega Man"
      },
      {
        "id": "COMPATIBLE_BRANDS",
        "value": "isommp42"
      },
      {
        "id": "SOFTWARE",
        "value": "Lavf57.20.100"
      }
    ]
  },
  "common": { // {{{
    "track": {
      "no": null,
      "of": null
    },
    "disk": {
      "no": null,
      "of": null
    },
    "artists": [
      "ABSRDST"
    ],
    "artist": "ABSRDST",
    "albumartist": "Mega Man"
  } // }}}
} */
// }}}
/* }}} */

// run when program starts
function startup() { // {{{
	var configDir, dbName, configPromise,
	    quiet = false;
	
	if (/inspect|analy[sz]e|query|playlist|regen(-lists|erate)|purge(-db)?/
		.test(process.argv[2]) === true)
	{
		configDir = KHip.defaults.configDir = util.resolveHome(KHip.defaults.configDir);
		KHip.dbMeta.filename = dbName = path.join(configDir, 'khip.db');
	}
	else {
		noArgs();
	}
	
	// if using a feature that requires the db,
	// do stuff after getting the db
	// khip analyse / query / playlist
	if (/analy[sz]e|query|playlist|regen(-lists|erate)|purge(-db)?/.test(process.argv[2]) === true) {
		
		// get config
		// right now, only analyse needs it
		configPromise = getConfig();
	
		createDB(dbName).then(function(db) {
			
			KHip.db = db;
			configPromise.then(() => {
			
			if (/analy[sz]e/.test(process.argv[2]) === true && process.argv[3] !== undefined) {
				KHip.inputDir = process.argv[3];
				
				if (/--quiet|-q/.test(process.argv.join(' '))) {
					quiet = true;
				}
				
				analyse(process.argv[3], quiet);
			}
			else if (process.argv[2] == 'query') {
				queryTableOutput(process.argv[3], process.argv[4]);
			}
			else if (process.argv[2] == 'playlist') {
				playlistOutput(process.argv[3], process.argv[4]);
			}
			// khip regen-lists / regenerate
			else if (/regen(-lists|erate)/.test(process.argv[2]) === true) {
				regenerateLists();
			}
			else if (/purge(-db)?/.test(process.argv[2]) === true) {
				purgeDB();
			}
			
			},
			function (err) {
				console.error('getConfig promise error:', err);
			});
		
		}, function(err) {
			console.error('createDB then error:', err);
		});
	
	}
	// otherwise do stuff immediately
	else if (/inspect/.test(process.argv[2]) === true) {
		// khip inspect file.ext
		if (process.argv[2] == 'inspect') {
			KHip.inputDir = path.dirname(path.resolve(process.argv[3]));
			inspectOne(process.argv[3]);
		}
	}
                                         
} // }}}

/* wrappers {{{ */

// khip inspect
function inspectOne(filename) {
	var native;

	inspect(filename).then(function (relevantTags) {
		native = relevantTags.native;
		delete relevantTags.native;
		console.log(relevantTags);
		console.log(native);
	},
	function (err) {
		console.error(err.message);
	});
}

// khip analyse: analyse()

// khip query
function queryTableOutput(statement, columns) {
	//var
	
	if (columns === undefined && KHip.config.analyseColumns !== undefined) {
		columns = KHip.config.analyseColumns;
	}
	
	//onsole.log(KHip.config.analyseColumns, columns);
	
	outputTableOrPlaylist(statement, columns);
	
}

// khip playlist
function playlistOutput(statement, saveTo) {
	const columns = [
		'title', 'artist', 'khip:filepath', 'khip:filename'
	];
	
	if (saveTo === undefined) {
		saveTo = '';
	}
	
	outputTableOrPlaylist(statement, columns, saveTo);
	
}

// help function if you call khip without arguments
function noArgs() { // {{{
	
	console.log(
		'Usage: khip [mode] [options]\n',
		
		'\nAvailable modes:',
		'\n',
		
		'\nRead metadata\n',
		' khip mode "file/dir"\n',
		'\n',
		' inspect         debug metadata of a single file\n',
		' analyse         add a folder to database\n',
		'   --quiet (-q)  suppress track table\n',
		
		'\nQuery database\n',
		' khip mode "sql" ["col_a,col_b"]\n',
		'\n',
		' query           query the database\n',
		' playlist        generate a playlist\n',
		
		'\nMisc\n',
		' khip mode\n',
		'\n',
		' regenerate      regenerate lists in lists.cfg\n',
		' purge           empty database file\n',
		'\n',
		
		''
	);
} // }}}

/* }}} */


// inspect an individual file
// returns a Promise containing the tags
// BUG: some badly formed files make this func hang for a long time
// but I don't know how to debug it
function inspect(filename) { // {{{
	var relevantTags, filesize;
	
	if (!fs.existsSync(filename)) {
		throw new Error('file ' + filename + ' was not found');
	}
	
	// returning mm.parseFile().then returns a Promise
	return mm.parseFile(filename, { native: true })
	.then( (metadata) =>
	{
		//onsole.log('\n\n', metadata, '\n\n');
		
		if (metadata.format.dataformat === undefined) {
			throw decorate(new Error('file tags have empty format field'), [ filename, JSON.stringify(metadata) ]);
		}
		
		metadata.filename = path.basename(filename);
		metadata.filepath = path.dirname(filename);
		
		metadata.filesize = fs.statSync(filename).size;
		
		relevantTags = tagTable(metadata);
		
		return relevantTags;
		
	},
	// error should be caught in a wrapper like inspectOne / analyse
	(err) => {
		throw decorate(err, [ filename ]);
	});
	
} // }}}

// format tag object returned by inspect
function tagTable(tagArray) { // {{{
	var obj, tags, key, i;
	
	const tagMap = KHip.TAG_MAP;
	
	obj = {
		format: tagArray.format.dataformat,
		'native': {},
		fix: {
			'khip:format': tagArray.format.dataformat,
			'khip:filename': tagArray.filename,
			'khip:filepath': tagArray.filepath,
			'khip:filesize': tagArray.filesize,
		}
	}
	
	if (obj.format == 'mp3') {
		tags = tagArray['native']['ID3v2.4'];
	}
	else if (obj.format == 'Ogg\/Vorbis I') {
		tags = tagArray['native'].vorbis;
	}
	else {
		tags = [];
		obj.native = tagArray.native;
	}
	
	// if tags are empty return an empty set of tags
	if (tags === undefined) {
		tags = [];
	}
	
	
	for (i = 0; i < tags.length; i++) {
		// return native unchanged
		obj['native'][tags[i].id] = tags[i].value;
		key = tags[i].id;
		
		// if tag name has a more readable version use that
		if (tagMap[obj.format][tags[i].id] !== undefined) {
			key = tagMap[obj.format][tags[i].id];
		}
		// if a key starts with txxx remove it
		else if ((/^TXXX:/).test(key)) {
			key = tags[i].id.replace(/^TXXX:/, '').toLowerCase();
		}
		else {
			key = tags[i].id.toLowerCase();
		}
		
		// set tag value with whatever the key (tag name) is
		obj.fix[ key ] = tags[i].value;
		
		if (obj.format == 'mp3') {
			// comments in mp3 are an object
			// so should be flattened
			if (tags[i].id == 'COMM') {
				obj.fix[ 'comment' ] = tags[i].value.text;
			}
			
			// fix strings that end with null
			if (typeof tags[i].value == 'string') {
				obj.fix[ key ] = tags[i].value.replace(/\u0000$/, '');
			}
		}
	}
	
	// save youtube id
	if (/youtube/.test(obj.fix['wwwsource']) === true) {
		obj.fix['khip:youtube'] = obj.fix['wwwsource'].replace(/^https?:\/\/www\.youtube\.com\/watch\?v=([A-Za-z0-9_-]+)\&?.*/, '$1');
	}
	
	// if title is undefined use filename
	if (obj.fix.title === undefined) {
		obj.fix.title = obj.fix['khip:filename'];
	}
	
	// generate uuid
	if (obj.fix['khip:uuid'] === undefined) {
		obj.fix['khip:uuid'] = uuidv5(path.join(obj.fix['khip:filepath'], obj.fix['khip:filename']) + obj.fix['khip:filesize'], KHip.NAMESPACE)
	}
	
	return obj;
} // }}}


// khip analyse
// analyse a dir and all the folders inside recursively
// TODO: analyse progress bar
// BUG: some rows already in the db are trying to insert
// and deleting those rows only causes More rows to try to insert???
function analyse(dir, quiet) { // {{{
	var alter = '', insert = '', update = '', transaction;
		
	//onsole.log('quiet', quiet);
	
	if (/^\/|[A-Z]:\\/
		.test(dir) === false) {
		KHip.inputDir = dir = path.join(process.cwd(), dir);
	}
	
	process.chdir(dir);
	
	fetchUUID().then(function(data) {
		analyseDir(dir).then(function (sql) {
				
			/*console.log(
				'\nANALYSE BIG RESOLVE' ,
				'\ninsert:', sql.insert.length ,
				'\nalter:', sql.alter.length ,
				'\nupdate:', sql.update.length ,
				'\ntable:', sql.table.length
			);*/
			
			// construct transaction
			
			alter = alterStatement(sql.alter);
			insert = insertStatement(sql.insert, sql.columns);
			update = updateStatement(sql.update);
			
			transaction = ([ 'BEGIN EXCLUSIVE TRANSACTION;' , alter, insert, update, 'COMMIT' ]).join('\n\n').replace(/\n\n+/g, '\n\n');
			
			//onsole.log('columns at transaction', JSON.stringify(sql.columns), '\n');
			//onsole.log(insert);
			
			
			// if not --quiet/-q, print an analyseTable
			if (quiet !== true) {
				console.log('\n');
				analyseTable(sql.table);
			}
			
			// run sql statements
			// if there is an error, the whole transaction can be debugged
			runTransaction(transaction).then(
				function () {
					//onsole.log('transaction ok');
				},
				function (err) {
					console.log(transaction);
					console.log('transaction error:', err);
				}
			);
		
		},
		function (e) {
			console.log('analysedir then:', e);
		});
		
	}); // end 
	
	// 604, 179
	// 
	
} // }}}

// construct an ALTER TABLE statement to add columns to table
function alterStatement(data) { // {{{
	var query, columns = [], i;
	
	// this can take either a string of column name, or an array
	if (typeof data == 'string') {
		data = [ data ];
	}
	
	// filter columns for duplicates
	// BUG: this shouldn't be necessary.
	// there's a problem with analyseDir()
	for (i = 0; i < data.length; i++) {
		if (columns.indexOf(data[i]) === -1 && KHip.dbMeta.columns.indexOf(data[i]) === -1) {
			columns.push(data[i]);
		}
	}
	
	columns.toString = function() {
		var cols = [], i, columnType;
		
		const columnTypes = KHip.COLUMN_TYPES;
		
		for (i = 0; i < this.length; i++) {
			columnType = 'text';
		
			if (columnTypes[this[i]] !== undefined) {
				columnType = columnTypes[this[i]];
			}
			
			// it turns out you can't actually add multiple columns
			// when using sqlite, which is fuckin' dumb
			// when you can add 1000 rows at a time
			// but that's the way it is
			cols.push('ALTER TABLE tracks ADD "' + this[i] + '" ' + columnType + ';');
		}
		
		return cols.join('  ');
	};
	
	//query = 'ALTER TABLE tracks ADD ' + columns.join(', ') + ';';
	query = columns + '';
	
	return query;
} // }}}

// construct an insert statement for analyse
function insertStatement(inserts, columns) { // {{{
	var statement, values = [], value, i, j;
	    
	const maxInserts = 1000;
	
	/*
	INSERT INTO  MyTable  ( Column1, Column2, Column3 )
	VALUES ('John', 123, 'Lloyds Office'), ('Miranda', 126, 'Bristol Office'),
	VALUES ('Alice', 127, 'Foo Office'), ('Bob', 128, 'Bar Office');
	*/
	
	
	for (i = 0; i < inserts.length; i++) {
		value = [];
		
		for (j = 0; j < columns.length; j++) {
			value.push( sqlUtils.columnValue(inserts[i], columns[j]) )
		}
		
		value = '(' + value.join(',') + ')';
		
		values.push(value);
	}
	
	columns.toString = function() {
		return '(' + sqlUtils.columnList(this) + ')';
	};
	
	values.toString = function() {
		var i, statements = [];
		
		for (i = 0; i < this.length; i++) {
			statements.push('INSERT INTO tracks ' + columns + '\nVALUES ' + this[i] + ';');
		}
		
		return statements.join('\n');
	};
	
	statement = values + '';
	
	return statement;
} // }}}

function updateStatement(updates) { // {{{
	var statement, values = [], value, i, j;
	
	const columnTypes = KHip.COLUMN_TYPES;
	
	// UPDATE table_name  SET column1 = value1, column2 = value2, ...
	// WHERE condition;
	
	if (updates.length === 0) {
		return '';
	}
	
	for (i = 0; i < updates.length; i++) {
		value = [];
		
		for (j in updates[i]) {
			// since uuid is being used for WHERE, don't update it
			if (j == 'khip:uuid') {
				continue;
			}
			
			value.push('"' + j + '" = ' + sqlUtils.columnValue(updates[i], j) + '');
		}
		
		value = 'UPDATE tracks SET ' + value.join(', ') + ' WHERE "khip:uuid" is "' + updates[i]['khip:uuid'] + '";';
		
		values.push(value);
	}
	
	return values.join('\n') + '';
} // }}}

function decorate(error, extra) {
	return { error: error, extra: extra };
}

// analyse files after recursively scanning
function analyseDir(dirname) { // {{{
	var processCwd, tagsLength, key, dirLength,
		sqlStatements =
		{ alter: [], insert: [], update: [], table: [], columns: null },
		totalDirs = 0, scannedDirs = 0, columns,
		fileList,
		lineEater = '\033[1A',
		i, j;
		
	const uuid = KHip.dbMeta.uuid;
	
	// there were a lot of issues with both getting all the files
	// AND also resolving Promises properly
	// until I decided to make the list of files global
	// it will only be global while the analyse runs, then moved to private
	// (which could conceivably result in a garbage collect, which is good)
	KHip.analyseFiles = [];
	
	return new Promise((resolve, reject) => {
		
	columns = KHip.dbMeta.columns.slice(0);
	//onsole.log('pragma', JSON.stringify(columns), '\n');
	
	process.chdir(dirname);
	processCwd = process.cwd();
	
	columns = KHip.dbMeta.columns.slice(0);
	
	console.log('start analyse');
	
	// get all the files in the dir, to then inspect them linearly
	getDirFiles(dirname).then(() => {
		// free up global
		fileList = KHip.analyseFiles;
		delete KHip.analyseFiles;
		
		//onsole.log('filelist', fileList);
		
		// something very weird happens between the last inspect & first analyse
		// there's a pause of about 5 seconds.
		// not sure what's going on there
		
		for (i = 0; i < fileList.length; i++)
		{
			//onsole.log(lineEater + 'open track ' + (i+1) + '/' + fileList.length);
			
			if (i == fileList.length - 1) {
				console.log('retrieved ' + fileList.length + ' files');
				console.log('');
			}
			
			inspect(path.join(fileList[i].path, fileList[i].name)).then((relevantTags) =>
			{
				console.log(lineEater + 'analyse track ' + (sqlStatements.table.length + 1)  + '/' + fileList.length + '          ');
				
				if (relevantTags === undefined || relevantTags.fix === undefined) {
					throw decorate(new Error('tags not defined for file ' + (sqlStatements.table.length + 1)), [ relevantTags ]);
				}
				
				// add track to analysetable
				sqlStatements.table.push(relevantTags.fix);
				
				// if uuid is known to be in table
				// push to queue for UPDATE'ing
				if (uuid.indexOf(relevantTags.fix['khip:uuid']) !== -1) {
					sqlStatements.update.push(relevantTags.fix);
				}
				// otherwise push to queue for INSERTing
				else {
					sqlStatements.insert.push(relevantTags.fix);
				}
				
				
				for (j in relevantTags.fix) {
					key = j.toLowerCase().replace(/^txxx:(.+)$/, '$1');
					
					// add column to sql db
					if (columns.indexOf(key) === -1) {
						columns.push(key);
						sqlStatements.alter.push(key);
					}
				}
				
				if (sqlStatements.table.length === fileList.length) {
					//onsole.log('RESOLVING');
					sqlStatements.columns = columns;
					resolve(sqlStatements);
				}
				
			}, function (e) {
				console.log('inspect track error', e);
				console.log('\n');
			});
		}
		
	});
		
	}); // end analyse promise
	
} // }}}

// recursing section of analyseDir
function getDirFiles(dirname) { // {{{
	var fileList = [], i, processCwd, totalDirs = 0, scannedDirs = 0,
	    getFilesPromise, promises = [];
	    
	const checkFinish = function(promises, promisesNumber) {
		var i, finished = true;
		
		if (promises.length < promisesNumber
			|| dirname !== KHip.inputDir) {
			return false;
		}
		
		for (i = 0; i < promises.length; i++) {
			if (promises[i].finished !== true) {
				finished = false;
			}
		}
		
		return finished;
	};
	

	return new Promise((resolve, reject) => {

	//countFiles(dirname, (err, results) => {
				
	totalDirs = 0;
	scannedDirs = 0;
	
	process.chdir(dirname);
	processCwd = process.cwd();
			
	fs.readdir(processCwd, { withFileTypes: true }, (err, files) => {
	
		for (i = 0; i < files.length; i++)
		{
			// if file is a folder, analyse that folder
			if (files[i].isDirectory() === true) {
				//onsole.log('recurse to', files[i].name);
				
				totalDirs++;
				
				getFilesPromise = getDirFiles(path.join(processCwd, files[i].name));
				//getFilesPromise.name = files[i].name;
				//getFilesPromise.number = promises.length;
				//promises.push(getFilesPromise);
				
				getFilesPromise.then(() => {
					scannedDirs++;
					
					if (scannedDirs === totalDirs) {	
						//onsole.log('RESOLVED RESOLVED RESOLVED', dirname);
						resolve();
					}
				});
			}
			else if (/mp3|m4a|ogg|flac/.test(files[i].name) === true) {
				//fileList.push(files[i].name);
				KHip.analyseFiles.push({ name: files[i].name, path: processCwd } );
			}
			//if file is not a usable type, skip it
		}
		
		// if this is just an inner directory, end
		if (promises.length === 0 && totalDirs === 0) {
			//onsole.log('no promise. resolve ', processCwd);
			resolve();
		}
	
	}); // end readdir "then"
	//}); // end countfiles
	
	});
	
} // }}}


// query and output a table or a playlist
function outputTableOrPlaylist(statement, columns, playList) { // {{{
	var testColumn;
	
	//onsole.log('COLUMNS', columns);
	
	// if columns are given, such as
	// khip query '"tagging:pkmntype" like "%Fairy%"' 'title'
	// or an array of columns
	// use them
	if (columns !== undefined) {
		if (typeof columns == 'string') {
			columns = columns.replace(/"/g, '');
			columns = columns.split(',');
		}

		//statement = 'select ' + columns + ' from tracks where ' + statement;
		
		//onsole.log('table columns', columns);
	}
	else if (columns === undefined) {
		columns = [ 'title' ];
	}
	
	columns.toString = function() {
		var edit = [], i;
		
		for (i = 0; i < this.length; i++) {
			if (/^[A-Za-z_]+$/.test(this[i]) === false) {
				edit.push('"' + this[i] + '"');
			}
			else {
				edit.push(this[i]);
			}
		}
		
		return edit.join(',');
	}
	
	// extract columns from a statement like
	// khip query '"tagging:pkmntype" like "%Water%"'
	// TODO: test this for multiple columns.
	if (/^select/.test(statement) === false)
	{
		testColumn = statement.replace(/"?([^"]+)"?\s(like|is)\s.+/, '$1');
			
		if (columns.indexOf(testColumn) === -1) {
			columns.push(testColumn);
		}
		
		statement = 'select ' + columns + ' from tracks where ' + statement;
		
		//onsole.log('STATEMENT', statement);
	}
	
	// if there is a single statement starting with select,
	// just use it
	
	
	// if specified, output a playlist
	if (playList !== undefined) {
		query(statement).then(function (rows) {
			playlist(rows, columns, playList);
		},
		function(e) {
			console.log(e);
		});
	}
	// else output a table
	else {
		query(statement).then(function (rows) {
			analyseTable(rows, columns);
		},
		function(e) {
			console.log(e);
		});
	}
	
} // }}}

// generate a table of track info
// for analyse/query
function analyseTable(table, columns) { // {{{
	var consoleTable = [[]], headerArr,
	    maxColumnWidth = 30, columnWidth, i;
	
	if (columns !== undefined) {
		headerArr = columns;
	}
	else if (KHip.config.analyseColumns !== undefined) {
		headerArr = KHip.config.analyseColumns;
	}
	else {
		headerArr = KHip.defaults.analyseColumns;
	}
	
	//onsole.log('headerarr', JSON.stringify(headerArr));
	
	for (i = 0; i < headerArr.length; i++) {
		if (KHip.headershort[headerArr[i]] !== undefined) {
			consoleTable[0].push(KHip.headershort[headerArr[i]]);
		}
		else {
			consoleTable[0].push(headerArr[i]);
		}
	}
	
	
	// calculate max column width
	maxColumnWidth = Math.floor(process.stdout.columns / headerArr.length)
	- (Math.max(headerArr.length + 1, 0));
	
	for (i = 0; i < table.length; i++) {
		row = [];
		
		for (j = 0; j < headerArr.length; j++) {
			if (typeof table[i][headerArr[j]] == 'string')
			{
				columnWidth = maxColumnWidth;
				
				// double byte strings sometimes are wider, about 303/226 ratio
				// so adjust their max width to be lower
				if (util.isDoubleByte(table[i][headerArr[j]])) {
					columnWidth = Math.floor(maxColumnWidth * 0.7000);
				}
				
				row.push(table[i][headerArr[j]].substring(0, columnWidth));
			}
			else if (table[i][headerArr[j]] !== undefined) {
				row.push(table[i][headerArr[j]]);
			}
			else {
				row.push('');
			}
		}
		
		consoleTable.push(row);
	}
	
	console.log(table.length + ' tracks');
	console.log(Table.table(consoleTable));
} // }}}

// generate an m3u playlist from a table of tracks
// such as got from analyse or query
function playlist(rows, columns, saveTo) { // {{{
	var i, m3u, root, up;
	
	root = process.cwd().replace(/\/$/, '');
	up = path.relative(path.join(root, saveTo), root).replace(/^..(\/|$)/, '.$1');
	//onsole.log(root, saveTo, up);
	
	const stringify = function() {
		var filepath, info, artist = this['artist'];
		
		// if artist is empty, fill with something else
		if (this['artist'] === null) {
			// artist will only be null, but
			// albumartist can be null OR undefined
			if (this['albumartist'] !== null) {
				artist = this['albumartist'];
			}
			else if (this['album'] != null) {
				artist = this['album'];
			}
			else {
				artist = 'Unknown Artist';
			}
		}
		
		info = '#EXTINF:-1,' + artist + ' - ' + this['title'] + '\n';
		
		filepath = path.join(this['khip:filepath'], this['khip:filename']).replace(root, up);
		
		return info + filepath;
	};
	
	
	for (i = 0; i < rows.length; i++) {
		rows[i].toString = stringify;
	}
	
	rows.toString = function() {
		var header = '#EXTM3U\n', result;
		
		result = header + this.join('\n\n');
		
		return result;
	}
	
	m3u = rows.toString();
	
	if (saveTo !== '') {
		fs.writeFile(saveTo, m3u, (err) => {
		});
	}
	else {
		console.log(m3u);
	}
	
} // }}}

// regenerate all playlists specified in lists.cfg
function regenerateLists() { // {{{
	var lines, i,
	    params = {}, lists;
	
	console.log('Regenerating lists:');
	
	lists = KHip.config.lists;
	
	if (lists === undefined || lists.length < 1) {
		console.log('No lists. add them in lists.cfg');
		return;
	}
	
	// cd $KHIPROOT
	process.chdir(util.resolveHome(KHip.config.rootDir));
	
	// generate each list
	for (i = 0; i < lists.length; i++) {
		// khip playlist  '"tagging:pkmntype" like "%Bug%"' $KHIPLISTS/type_bug.m3u
		
		console.log('Creating playlist ' + lists[i].name + '.m3u');
		playlistOutput(lists[i].query, KHip.config.listDir + '/' + lists[i].name + '.m3u');
		//onsole.log('playlist output', lists[i].query, params['KHIPLISTS'] + '/' + lists[i].name + '.m3u');
		
		//break;
	}
	
	
} // }}}


/* sql queries {{{ */

// note: don't have a db viewer open while running khip
// or the db will be """busy"""

// create or open sqlite db
function createDB(dbName) { // {{{
	var configDir = KHip.defaults.configDir;
	
	const
	defaultColumns = KHip.defaults.dbColumns,
	
	createTable = (db, resolve) => {
		db.run('create table tracks( ' + sqlUtils.columnList(defaultColumns, sqlUtils.COLUMN_TYPES) + ', CONSTRAINT uuid UNIQUE ("khip:uuid") );',
		function(err) {
			if (err) throw err;
			
			KHip.dbMeta.columns = defaultColumns.slice(0);
			resolve(db);
		});
	};
	
	//onsole.log('create table tracks( ' + sqlUtils.columnList(defaultColumns) + ', CONSTRAINT uuid UNIQUE ("khip:uuid") );');
	
	
	return new Promise((resolve, reject) => {
		var db;
			
	try {     
		
		if (fs.existsSync(configDir) === false) {
			fs.mkdirSync(configDir);
		}
	
		if (fs.existsSync(dbName) === false) {
			//onsole.log('creating db', dbName);
		
			db = new sqlite3.Database(dbName, sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE);
			
			createTable(db, resolve);
		}
		else {
			//onsole.log('found db', dbName);
			
			db = new sqlite3.Database(dbName, sqlite3.OPEN_READWRITE);
			
			// if tracks table has been dropped,
			// such as in khip purge
			// recreate it
			db.get('SELECT * from tracks limit 1', (pragmaerr, data) => {
				if (pragmaerr && /no such table/.test(pragmaerr.message)) {
					createTable(db, resolve);
					//onsole.log('table created');
					return;
				}
				else if (pragmaerr) {
					console.log('test tracks table error:', pragmaerr.message);
					return;
				}
				
				//onsole.log('table found');
					
				db.all('PRAGMA table_info(tracks)', (pragmaerrtwo, data) => {
					var columns = [], i;
					
					if (pragmaerrtwo) {
						console.log('error getting columns:', pragmaerrtwo);
					}
	
					for (i = 0; i < data.length; i++) {
						columns.push(data[i].name);
					}
					
					KHip.dbMeta.columns = columns;
						
					resolve(db);
				});
			});
		}
	
	}
	catch (e) {
		reject(e);
	}
	
	});
	
} // }}}

// clear tracks table data by deleting and remaking it
function purgeDB() { // {{{
	const dbName = KHip.dbMeta.filename;
	
	KHip.db.run('DROP TABLE tracks', function (err) {
		if (err) {
			console.error('purgedb error:', err);
			return;
		}
		
		console.log('data of ' + dbName + ' cleared');
		createDB(dbName);		
	});
	
} // }}}

// query the sqlite database,
// for returning a table or playlist
function query(statement) { // {{{
	
	//onsole.log('query', [ statement ]);
	
	return new Promise((resolve, reject) => {
		try {
			if (/^select/.test(statement) !== true) {
				throw new Error('query is not a select statement. check it carefully.');
			}
			
			KHip.db.all(statement, [], (err, rows) => {
				if (err) {
					throw err;
				}
				else if (rows === undefined) {
					throw new Error('rows is undefined');
				}
				
				//onsole.log('rows', rows);
				resolve(rows);
			});
		}
		catch (e) {
			reject(e);
		}
	});
	
} // }}}

// run a series of sql statements as a sql "transaction"
// these...... are somewhat limited in sqlite, but do work
function runTransaction(transaction) { // {{{
	var runStatements;
	
	// avoid splitting statements at semicolons in strings
	// this caused me some really baffling errors.
	// I thought it was the sqlite module acting up for a moment
	transaction = transaction.replace(/\n/g, '').replace(/([^\\]);/g, '$1::::::::::').split('::::::::::');
	
	return new Promise((transResolve, transReject) => {
		KHip.db.serialize(function() {
			var i, j = 0;
			
			for (i = 0; i < transaction.length; i++) {
				if (transaction[i] === '') {
					continue;
				}
				
				// correct escaped semicolons
				transaction[i] = transaction[i].replace(/\\;/g, ';');
				
				// sanitise bizarre uses of null to the unicode picture
				transaction[i] = transaction[i].replace(/\u0000/g, '\u2400');
				
				//onsole.log([ transaction[i] ]);
				
				KHip.db.run(transaction[i], (transerror) => {
					if (transerror) {
						transReject(transerror);
					}
					
					runStatements++;
					
					// resolve the transaction exactly after the last one
					if (runStatements == transaction.length) {
						transResolve();
					}
				});
			}
			
		});
	});
	
} // }}}

// fetch uuid array from table to check if row is new
function fetchUUID() { // {{{
	var i, uuids = [];
	
	return new Promise((resolve, reject) =>
	{	
		KHip.db.all('SELECT "khip:uuid" from tracks', function(err, data)
		{
			for (i = 0; i < data.length; i++) {
				uuids.push(data[i]['khip:uuid']);
			}
			
			//onsole.log('uuid', uuids);
			
			KHip.dbMeta.uuid = uuids;
			resolve(uuids);
		});
	});
	
} // }}}

// get config file
function getConfig() { // {{{
	var lines, i,
	    params = {}, lists = [],
	    analyseColumns;
	
	const filename = path.join(KHip.defaults.configDir, 'lists.cfg');
	
	
	return new Promise((resolve, reject) => {
		fs.readFile(util.resolveHome(filename), 'utf8', (err, data) => {
			if (err) reject(err);
			
			if (!data) {
				throw new Error('lists.cfg is empty');
			}
			
			// TODO: if lists.cfg is empty copy the default one
			
			//onsole.log('getconfig resolved');
			
			data = data.replace(/\n+/g, '\n');
			lines = data.split('\n');
			
			// parse lines of file
			for (i = 0; i < lines.length; i++) {
				// if line has an = split it at the = & use as param
				if (/=/.test(lines[i])) {
					lines[i] = lines[i].replace(/\s+=\s+/g, '=');
					lines[i] = lines[i].split('=');
					
					// collect param into object
					params[lines[i][0]] = lines[i][1]
					//onsole.log('PARAM: ', lines[i]);
				}
				// if it's not empty or a comment use as a list line
				else if (/^#|^\s*$/.test(lines[i]) === false) {
					// list lines can be indented however much
					// or have however many spaces between name & query
					lines[i] = lines[i].replace(/^\s*([A-Za-z0-9_-]+)\s+/g, '$1::::::::::');
					lines[i] = lines[i].split('::::::::::');
					lines[i][1] = lines[i][1].replace(/^'(.+)'$/, '$1');
					
					lists.push({ name: lines[i][0], query: lines[i][1] });
					//onsole.log('LIST: ', lines[i]);
				}
			}
			
			// parse params
			params['KHIPROOT'] = util.resolveHome(params['KHIPROOT']);
			params['KHIPLISTS'] = params['KHIPLISTS'].replace(/^\.\//, '');
			
			analyseColumns = params['ANALYSECOLUMNS'];
			
			if (analyseColumns !== undefined) {
				analyseColumns = analyseColumns.split(',');
				
				for (i = 0; i < analyseColumns.length; i++) {
					analyseColumns[i] = analyseColumns[i].replace(/^"(.+)"$/, '$1');
				}
			}
			//
			
			KHip.config = {
				rootDir: params['KHIPROOT'],
				listDir: params['KHIPLISTS'],
				analyseColumns: analyseColumns,
				
				lists: lists
			}
			
			//onsole.log(KHip.config);
			
			resolve();
		});
	});
	
} // }}}

/* }}} */




startup();

