# use mid3v2 to set extended tags
# this works better than the id3v2 command
# which discards all TXXX when you set just one

filename="$1"
extname="$2"
extvalue="$3"

# example correct output: TXXX=rating:noskip=bats

mid3v2 --TXXX "$extname:$extvalue" --escape "$filename"
