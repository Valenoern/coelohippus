#!/bin/bash
# a script to copy a list of tracks at absolute file urls to a single folder
# i use this to select a subset of music to put onto a limited size SD card
# usage: copy-tracks.sh playlist.m3u destination

# :folding=explicit: <- folds code in jedit
playlist=$1
dest=$2
optional=$3
startdir=`pwd`

function main {
 # you can use "return" in a bash script to give a return status
 # so && quits on error as with a system command
 checklist && checkdest && testsize && copyfiles
}

# check playlist exists
function checklist { # {{{
if [ -f "$playlist" ]; then
 return 0
else
 echo "Playlist not found"
 return 1
fi
} # }}}

# make sure destination dir exists & is in consistent format
function checkdest { # {{{
if [ -d "$dest" ]; then
 # normalise destination
 cd "$dest"
 dest=`pwd`
 cd "$startdir"
 return 0
else
 echo "Destination dir not found"
 return 1
fi
} # }}}

# test if enough space available
function testsize { # {{{
 echo "Checking if enough space is available"
 
 # reset grand size total
 totalsize="0"
 # get available space on device (tail removes the headers of the table)
 available=`df "$dest" -B1 --output=avail | tail -1`
 
 # go through every line of playlist file
 while read line
 do
  # if grep finds a match iscomment will be non-empty
  iscomment=`echo "$line" | grep "#"`
  if [ -z "$iscomment" ]; then
    # add size of file to total
    sizeincr=`du -b "$line" | sed "s/\([0-9]\+\).\+/\1/"`
    totalsize=`expr "$sizeincr" + "$totalsize"`
  fi
 done < "$playlist"
 
 # subtract total size from free space
 difference=`expr "$available" - "$totalsize" - 10`
 
 # return result of test to be used by &&
 if [[ "$difference" -ge "0" ]]; then
   echo "Free space after copy: $difference bytes"
   return 0
 else
   echo "Not enough free space"
   return 1
 fi
} # }}}

# copy files in playlist to destination
function copyfiles { # {{{
 # allow just showing how much free space
 # i was not sure how to use the proper --argument format.
 if [[ "$optional" == "dry-run" ]]; then
   echo "Not copying any files, 'dry-run' specified"
   return 0
 else
 
 echo "Copying files"
 
 # go through every line of playlist file
 while read line
 do
  # if grep finds a match iscomment will be non-empty
  iscomment=`echo "$line" | grep "#"`
  if [ -z "$iscomment" ]; then
    # copy file to destination
    cp -a "$line" "$dest/"
  fi
 done < "$playlist" &&
 
 echo "Done copying"
 
 fi
} # }}}

main